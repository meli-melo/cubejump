# Cube Jump

This is a game I developed by myself to test the Unity ads system.
I was inspired by mobile minigames playable with only one finger.

## Getting Started

First download this repository and then open it in Unity (2019.3 or higher).

### Prerequisites

To use this project on an Android device you will need to install Unity with all the Android development dependencies available in the installer.


## Built With

* Build the project for Android device from the Unity build window
* Copy the .apk file generated to your Android device
* Run the .apk on your device to install the game

* You can also build directly to your Android device from Unity

## Authors

* **Jean Bourquard** - *Whole project* - [meli-melo](https://gitlab.com/meli-melo)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details