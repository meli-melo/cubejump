﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    private UiManager _uiManager;
    [SerializeField]
    private SettingsManager _settingsManager;
    [SerializeField]
    private bool _isPaused = true;
    [SerializeField]
    private int _startSequenceDuration = 3;
    [SerializeField]
    private TMPro.TextMeshProUGUI _countdownText;
    [SerializeField]
    private PlayerController _playerController;
    [SerializeField]
    private killZoneController _killZoneController;
    [SerializeField]
    private CameraController _camController;
    [SerializeField]
    private PlatformManager _platformManager;

    [SerializeField]
    private TMPro.TextMeshProUGUI _scoreText;
    [SerializeField]
    private int _score = 0;
    [SerializeField]
    private enum EGameState { MAINMENU, SETTINGS, GAME, COUNTDOWN, GAMEOVER};
    [SerializeField]
    private EGameState _gameState;

#if UNITY_ANDROID
    private string gameId = "3536033";
    private bool testMode = false;
#endif

    // Start is called before the first frame update
    void Start()
    {
        _gameState = EGameState.MAINMENU;
        _uiManager.ShowMainMenuScreen(true);
        _uiManager.ShowGameScreen(false);
        _uiManager.ShowSettingsMenuScreen(false);
#if UNITY_ANDROID
        Advertisement.Initialize(gameId, testMode);
#endif
        _score = 0;
        UpdateScoreUI();
        _playerController.playerDie += PlayerDied;
        _playerController.reachedPlatform += PlayerReachedPlatform;
        _settingsManager.backFromSettings += CloseSettingsMenu;

        _camController.SetPlayerObject(_playerController.gameObject);
        _camController.MoveToPlayer();
    }

    // Update is called once per frame
    void Update()
    {
        if(_isPaused)
        {
#if UNITY_EDITOR
            if (Input.GetKeyDown(KeyCode.Space))
            {
                StartGame();
            }
#endif
#if UNITY_ANDROID
            if (Input.touchCount > 0)
            {
                StartGame();
            }
#endif
        }
    }

    public void StartGame()
    {
        _isPaused = false;
        _uiManager.ShowMainMenuScreen(false);
        _uiManager.ShowGameScreen(true);
        _uiManager.ShowGameOverText(false);
        StartCoroutine(StartSequence(_startSequenceDuration));
    }

    IEnumerator StartSequence(int duration)
    {
        _gameState = EGameState.COUNTDOWN;
        _countdownText.gameObject.SetActive(true);
        int count = 0;
        while(count < duration)
        {
            _countdownText.text = (duration - count).ToString();
            yield return new WaitForSeconds(1);
            count++;
        }
        _countdownText.gameObject.SetActive(false);
        _playerController.SetGameStart(true);
        _killZoneController.SetGameStart(true);
        _platformManager.StartSpawnSequence();
        _gameState = EGameState.GAME;
    }

    private void PlayerDied()
    {
        _gameState = EGameState.GAMEOVER;
        _playerController.SetGameStart(false);
        _playerController.GetComponent<MeshRenderer>().enabled = false;
        _playerController.transform.Find("Explosion_PS").gameObject.SetActive(true);
        _killZoneController.SetGameStart(false);
#if UNITY_ANDROID
        //Advertisement.Show("PlayerDiedTwice");
#endif
        StartCoroutine(DeathSequence());
    }

    IEnumerator DeathSequence()
    {
        _uiManager.ShowGameOverText(true);
        yield return new WaitForSeconds(2f);
        SceneManager.LoadSceneAsync("Game");
        _platformManager.RemovePlatforms(_playerController.GetPreviousPlatform());
        _killZoneController.transform.position = new Vector3(0, _playerController.transform.position.y - 5, 0);
        _uiManager.ShowMainMenuScreen(true);
        _uiManager.ShowGameScreen(false);
        _isPaused = true;
    }

    private void PlayerReachedPlatform()
    {
        _camController.MoveToPlayer();
        _score++;
        UpdateScoreUI();
        _platformManager.StartSpawnSequence();
    }
    private void UpdateScoreUI()
    {
        _scoreText.text = "Score: " + _score;
    }

    public void OpenSettingsMenuScreen()
    {
        if(_gameState != EGameState.SETTINGS)
        {
            _gameState = EGameState.SETTINGS;
            _uiManager.ShowMainMenuScreen(false);
            _uiManager.ShowSettingsMenuScreen(true);
            _playerController.playerDie -= PlayerDied;
            _playerController.reachedPlatform -= PlayerReachedPlatform;
        }
        else
        {
            _gameState = EGameState.MAINMENU;
            _uiManager.ShowMainMenuScreen(true);
            _uiManager.ShowSettingsMenuScreen(false);
        }
    }

    public void CloseSettingsMenu(GameObject chosenPlayer)
    {
        _playerController = chosenPlayer.GetComponent<PlayerController>();
        _playerController.playerDie += PlayerDied;
        _playerController.reachedPlatform += PlayerReachedPlatform;
        _camController.SetPlayerObject(_playerController.gameObject);
        _gameState = EGameState.MAINMENU;
        _uiManager.ShowMainMenuScreen(true);
        _uiManager.ShowSettingsMenuScreen(false);
    }
}
