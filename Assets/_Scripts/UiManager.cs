﻿using UnityEngine;

public class UiManager : MonoBehaviour
{
    [SerializeField]
    private GameObject _mainMenuScreen;
    [SerializeField]
    private GameObject _gameScreen;
    [SerializeField]
    private GameObject _settingsMenuScreen;
    [SerializeField]
    private GameObject _gameOverText;

    public void ShowMainMenuScreen(bool visibility)
    {
        _mainMenuScreen.SetActive(visibility);
    }

    public void ShowGameScreen(bool visibility)
    {
        _gameScreen.SetActive(visibility);
    }

    public void ShowSettingsMenuScreen(bool visibility)
    {
        _settingsMenuScreen.SetActive(visibility);
    }

    public void ShowGameOverText(bool visibility)
    {
        _gameOverText.SetActive(visibility);
    }
}
