﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformManager : MonoBehaviour
{
    [SerializeField]
    private GameObject[] _platformPool;
    [SerializeField]
    private bool _gameStart = false;
    [SerializeField]
    private Vector3 _prevPlatformPos = new Vector3(0f, 0.444f, 0f);
    [SerializeField]
    private GameObject _startingPlatform;
    [SerializeField]
    private List<GameObject> _instantiatedPlatforms;


    // Start is called before the first frame update
    void Start()
    {
        _instantiatedPlatforms = new List<GameObject>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!_gameStart)
            return;
    }

    public void SetGameStart(bool gameStart)
    {
        _gameStart = gameStart;
    }

    public void StartSpawnSequence()
    {
        StartCoroutine(SpawnPlatform());
    }

    IEnumerator SpawnPlatform()
    {
        Debug.Log("Spawn new platform");
        float rndTimer = Random.Range(1.1f, 1.4f);
        yield return new WaitForSeconds(rndTimer);
        int rndPlatform = Random.Range(0, _platformPool.Length);
        float rndPos = Random.Range(_prevPlatformPos.y + 2, _prevPlatformPos.y + 3);

        GameObject platform = Instantiate(_platformPool[rndPlatform], new Vector3(0f, rndPos, 0f), Quaternion.identity);
        _instantiatedPlatforms.Add(platform);
        _prevPlatformPos = platform.transform.position;
    }

    public void RemovePlatforms(GameObject currentPlatform)
    {
        _instantiatedPlatforms.Remove(currentPlatform);
        for(int i = 0; i < _instantiatedPlatforms.Count; i++)
        {
            Destroy(_instantiatedPlatforms[i]);
        }
        _instantiatedPlatforms.Clear();
    }
}
