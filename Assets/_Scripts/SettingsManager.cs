﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingsManager : MonoBehaviour
{
    [SerializeField]
    private UiManager _uiManager;
    [SerializeField]
    private GameObject[] _playerModels;
    [SerializeField]
    private GameObject _chosenPlayerModel;
    [SerializeField]
    private int _selectedPlayerModelIndex = 0;
    [SerializeField]
    private Quaternion _initRot;

    public delegate void BackFromSettings(GameObject chosenPlayer);
    public BackFromSettings backFromSettings;

    public void PreviousModelButtonClicked()
    {
        if(_selectedPlayerModelIndex > 0)
        {
            _selectedPlayerModelIndex--;
        } 
        else
        {
            _selectedPlayerModelIndex = _playerModels.Length - 1;
        }
        Vector3 playerPos = _chosenPlayerModel.transform.position;
        DestroyImmediate(_chosenPlayerModel);

        _chosenPlayerModel = Instantiate(_playerModels[_selectedPlayerModelIndex], playerPos, Quaternion.identity);
    }

    public void NextModelButtonClicked()
    {
        if(_selectedPlayerModelIndex < (_playerModels.Length - 1))
        {
            _selectedPlayerModelIndex ++;
        }
        else
        {
            _selectedPlayerModelIndex = 0;
        }
        Vector3 playerPos = _chosenPlayerModel.transform.position;
        DestroyImmediate(_chosenPlayerModel);

        _chosenPlayerModel = Instantiate(_playerModels[_selectedPlayerModelIndex], playerPos, _initRot);
    }

    public void BackButtonClicked()
    {
        backFromSettings?.Invoke(_chosenPlayerModel);
    }
}
