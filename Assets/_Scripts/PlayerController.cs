﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    private bool _gameStart = false;
    [SerializeField]
    private bool _isGrounded = true;
    [SerializeField]
    private Rigidbody _rb;
    [SerializeField]
    private Vector3 _jumpForce;
    [SerializeField]
    private ForceMode _jumpMode;
    [SerializeField]
    private float _jumpRaycastDist = 0.5f;
    [SerializeField]
    private float _fallMultiplier = 1.5f;
    [SerializeField]
    private GameObject _previousPlatform;
    [SerializeField]
    private GameObject _nextPlatform;

    public delegate void PlayerDie();
    public PlayerDie playerDie;

    public delegate void ReachedPlatform();
    public ReachedPlatform reachedPlatform;

    // Start is called before the first frame update
    void Start()
    {
        _isGrounded = true;
        _gameStart = false;
    }

    // Update is called once per frame
    void Update()
    {
        //prevent player movements if game is not started yet
        if (!_gameStart)
            return;


        if(!_isGrounded)
        {
            // RaycastHit hit;
            // if(Physics.Raycast(transform.position, Vector3.down, out hit, _jumpRaycastDist))
            // {
            //     Debug.Log("back to ground");
            //     _isGrounded = true;
            //     _nextPlatform = hit.collider.gameObject;

            //     if (_nextPlatform != _previousPlatform)
            //     {
            //         Debug.Log("Reached new platform");
            //         reachedPlatform?.Invoke();
            //     }            
            // }

            if(_rb.velocity.y < 0)
            {
                _rb.velocity += Vector3.up * Physics2D.gravity.y * _fallMultiplier * Time.deltaTime;
            }
        }
        else
        {
#if UNITY_EDITOR
            if (Input.GetKeyDown(KeyCode.Space))
            {
                RaycastHit hit;
                if (Physics.Raycast(transform.position, Vector3.down, out hit, _jumpRaycastDist))
                {
                    _previousPlatform = hit.collider.gameObject;
                }

                Debug.Log("Jump " + _previousPlatform.name);
                _rb.AddForce(_jumpForce, _jumpMode);
                _isGrounded = false;
            }
#endif
#if UNITY_ANDROID
            if(Input.touchCount > 0)
            {
                RaycastHit hit;
                if (Physics.Raycast(transform.position, Vector3.down, out hit, _jumpRaycastDist))
                {
                    _previousPlatform = hit.collider.gameObject;
                }


                Debug.Log("Jump " + _previousPlatform.name);
                _rb.AddForce(_jumpForce, _jumpMode);
                _isGrounded = false;
            }
#endif
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Killzone")
        {
            playerDie?.Invoke();
        }       

        if(!_isGrounded)
        {
            _isGrounded = true;
            Debug.Log("back to ground");
            if(other.gameObject.tag == "Platform")
            {
                if(other.gameObject != _previousPlatform)
                {
                    Debug.Log("Reached new platform " + other.gameObject.name);
                    _previousPlatform = other.gameObject;
                    reachedPlatform?.Invoke();
                }
            }
        }
    }

    public void SetGameStart(bool gameStart)
    {
        _gameStart = gameStart;
    }

    public GameObject GetPreviousPlatform()
    {
        return _previousPlatform;
    }
}
