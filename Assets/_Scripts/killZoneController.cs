﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class killZoneController : MonoBehaviour
{
    [SerializeField]
    private bool _gameStart = false;

    [SerializeField]
    public float _risingSpeed = 0.1f;
    // Start is called before the first frame update
    void Start()
    {
        _gameStart = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(_gameStart)
            transform.position += new Vector3(0, Time.deltaTime * _risingSpeed, 0);
    }
    
    public void SetGameStart(bool gameStart)
    {
        _gameStart = gameStart;
    }
}
