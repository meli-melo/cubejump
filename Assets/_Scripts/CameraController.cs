﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField]
    private GameObject _player;
    [SerializeField]
    private float _camSpeed = 0.5f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void SetPlayerObject(GameObject player)
    {
        _player = player;
    }

    public void MoveToPlayer()
    {
        StartCoroutine(LerpToPlayer());
        //transform.position = new Vector3(transform.position.x, _player.transform.position.y + 1.5f, transform.position.z);
    }

    IEnumerator LerpToPlayer()
    {
        float fration = 0f;
        Vector3 startPos = this.transform.position;
        Vector3 destPos = new Vector3(transform.position.x, _player.transform.position.y + 1.5f, transform.position.z);
        while (fration < 1f)
        {
            fration += Time.deltaTime * _camSpeed;
            transform.position = Vector3.Lerp(startPos, destPos, fration);
            yield return null;
        }
    }
}
